package com.example.demo.services;
import com.example.demo.models.Point;
import com.example.demo.models.RegresionEquation;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LinearRegresionService {

    public RegresionEquation calcularLinearRegresion(List<Point> pointList) {
        validarLista(pointList);
        validatePoints(pointList);
        double sumX = sumOfX(pointList);
        double sumY = sumOfY(pointList);
        double sumXY = sumOfXY(pointList);
        double sumXX = sumOfXX(pointList);
        int cant = pointList.size();
        double pendant = ((cant * sumXY) - (sumX * sumY))
                / ((cant * sumXX) - (sumX * sumX));
        double interception = (sumY - (pendant * sumX))
                / cant;

        return new RegresionEquation(interception, pendant);
    }

public double sumOfX(List<Point> pointList){
    return pointList.stream().mapToDouble(Point::getX).sum();
}
public double sumOfY(List<Point> pointList){
    return pointList.stream().mapToDouble(Point::getY).sum();
}

public double sumOfXY(List<Point>pointList){
return pointList.stream().mapToDouble(point-> point.getX()* point.getY()).sum();
}
    public double sumOfXX(List<Point>pointList){
        return pointList.stream().mapToDouble(point-> point.getX()* point.getX()).sum();
    }

    public void validatePoints(List<Point> points) {
        if (points == null || points.isEmpty()) {
            throw new IllegalArgumentException("la lista de puntos puede estar vacia");
        }
    }

public void validarLista(List<?> list){
for (Object object:list){
    if (!(object instanceof Point)){
        throw new IllegalArgumentException("la lista debe solo objetos tipo point");
    }
}
}



}
