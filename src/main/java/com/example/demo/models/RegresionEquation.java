package com.example.demo.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.text.DecimalFormat;

@AllArgsConstructor
@Setter
@Getter
public class RegresionEquation {
    private double pendant;
    private double interception;

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.###");
        return "y = " + df.format(pendant)+"+"+ df.format(interception)+"x";
    }

}
