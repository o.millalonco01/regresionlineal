package com.example.demo.controllers;

import com.example.demo.models.Point;
import com.example.demo.services.LinearRegresionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class LinearRegresionController {
    @Autowired
    LinearRegresionService linearRegresionService;

@PostMapping("/regresion-linear")
    public String getLinearRegresion(@RequestBody List<Point> points){
    return linearRegresionService.calcularLinearRegresion(points).toString();
}

}
