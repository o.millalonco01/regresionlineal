package com.example.demo.services;

import com.example.demo.models.Point;
import com.example.demo.models.RegresionEquation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class LinearRegresionServiceTest {
    @Autowired
    private LinearRegresionService linearRegresionService;

    private List<Point> pointList;

    @BeforeEach
    void setUp() {
        linearRegresionService = new LinearRegresionService();
        pointList = new ArrayList<>();
        pointList.add(new Point(0, 0.692847));
        pointList.add(new Point(1, 0.224983));
        pointList.add(new Point(2, 0.574601));
        pointList.add(new Point(3, 0.624027));
        pointList.add(new Point(4, 0.097651));
        pointList.add(new Point(5, 0.071073));
        pointList.add(new Point(6, 0.133361));
        pointList.add(new Point(7, 0.206914));
        pointList.add(new Point(8, 0.961989));
        pointList.add(new Point(9, 0.438110));
        pointList.add(new Point(10, 0.059712));
        pointList.add(new Point(11, 0.912941));
        pointList.add(new Point(12, 0.894481));
        pointList.add(new Point(13, 0.521333));

    }


    @Test
    void validate() {
        for (Point point : pointList) {
            assertInstanceOf(Double.class, point.getX(), "x no es double");
            assertInstanceOf(Double.class, point.getY(), "y no es double");
        }
    }

    @Test
    void validarListaSoloPuntos() {
        List<Object> objetosIncorrectos = new ArrayList<>();
        objetosIncorrectos.add(new Point(0, 0));
        objetosIncorrectos.add("cadena");
        objetosIncorrectos.add(123);
        assertThrows(IllegalArgumentException.class, () -> this.linearRegresionService.validarLista(objetosIncorrectos));
    }

    @Test
    void sumOfXTest() {
        double expected = 91;
        double actual = pointList.stream().mapToDouble(Point::getX).sum();
        assertEquals(expected, actual);
    }

    @Test
    void sumOfYTest() {
        double expected = 6.414023;
        double actual = pointList.stream().mapToDouble(Point::getY).sum();
        System.out.println("que pasoo" + actual);
        assertEquals(expected, actual);
    }

    @Test
    void sumOfXY() {
        double expected = 46.030273;
        double actual = pointList.stream().mapToDouble(point -> point.getX() * point.getY()).sum();
        assertEquals(expected, actual);
    }
    @ParameterizedTest
    @CsvSource({
            "91.0, 6.414023"
    })
    void sumOfXYParam(double expectedSumX, double expectedSumY) {
        double actualSumX = linearRegresionService.sumOfX(pointList);
        double actualSumY = linearRegresionService.sumOfY(pointList);
        System.out.println("la suma son X: "+actualSumX+"/ Y: "+actualSumY);
        System.out.println("suma de atributos"
                +pointList.stream().mapToDouble(point->point.getX()*point.getY()).sum());
        assertEquals(expectedSumX, actualSumX);
        assertEquals(expectedSumY, actualSumY);
    }

    @Test
    void sumOfXX(){
        double expected =819;
        double actual= pointList.stream().mapToDouble(point->point.getX()*point.getX()).sum();
        System.out.println("resultado de X al cuadrado"+actual);
        assertEquals(expected,actual);
    }


    @Test
    void calcularLinearRegresion() {
        RegresionEquation result = linearRegresionService.calcularLinearRegresion(pointList);
        RegresionEquation expected = new RegresionEquation(0.334, 0.019);
        System.out.println("resultado: "+result+"\n esperado: "+expected);
        assertEquals(expected.toString(), result.toString());
    }

}